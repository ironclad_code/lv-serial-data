const unsigned int MAX_MESSAGE_LENGTH = 12;

void setup() {
  Serial.begin(9600);
  pinMode(13, OUTPUT);
  pinMode(12, OUTPUT);
  digitalWrite(13, LOW);
  digitalWrite(12, LOW);
}

void loop() {
  //Check to see if anything is available in the serial receive buffer
  while (Serial.available() > 0) {
   //Create a place to hold the incoming message
   static char message[MAX_MESSAGE_LENGTH];
   static unsigned int message_pos = 0;

   //Read the next available byte in the serial receive buffer
   char inByte = Serial.read();

   //Message coming in (check not terminating character) and guard for over message size
   if ( inByte != '\n' && (message_pos - MAX_MESSAGE_LENGTH - 1) ) {
     //Add the incoming byte to our message
     message[message_pos] = inByte;
     message_pos++;
   }
   //Full message received...
   else {
     //Add null character to string
     message[message_pos] = '\0';

     //Print the message (or do other things)
     Serial.println(message);
     String message_string = message;

     if(message_string.equals("green on")) {
      digitalWrite(13, HIGH);
     }

     if(message_string.equals("green off")) {
      digitalWrite(13, LOW);
     }

    if(message_string.equals("blue on")) {
      digitalWrite(12, HIGH);
     }

     if(message_string.equals("blue off")) {
      digitalWrite(12, LOW);
     }
     if(message_string.equals("all off")) {
      digitalWrite(12, LOW);
      digitalWrite(13, LOW);
     }

     //Reset for the next message
     message_pos = 0;
   }
 }
}
